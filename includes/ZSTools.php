<?php


/**
 * Class ZSTools
 */
class ZSTools
{
    const EMM_ZS_EXTENSION_NAME = "EMM-ZS-extension";
    const FILES_DIR = "files";

    public static function onParserFirstCallInit(Parser $parser ) {
		$parser->setHook( 'emm-zs-tag', 'ZSTools::renderEmmZsTag' );
		return true;
	}
	
public static function onBeforePageDisplay( $output, $skin ) {
 

        $output->addModules( 'ext.EMM-ZS-Extension' );
        
    }


    /** get dir inside extension to save downloaded files into
     * @return string
     */
    public static function getFileStoreDir() {
        global $wgExtensionDirectory;
        $extensionDir = $wgExtensionDirectory . "/". self::EMM_ZS_EXTENSION_NAME ."/". self::FILES_DIR ."/";
        return $extensionDir;
    }

    public static function getPBZpath($data) {
        //$replaced = str_replace("/sites/default/files/styles", "https://publicaties.dezb.nl/sites/default/files/styles", $data);
        //$replaced = str_replace("https://publicaties.dezb.nlhttps://publicaties.dezb.nl", "https://publicaties.dezb.nl", $replaced);
        return $data;
    }
        /**
     * get contents of page-part of PBZ-report
     * @param $pageId
     * @param $pageKind
     * @param Parser $parser
     * @param PPFrame $frame
     * @return array|string|string[]|null
     */
    public static function getContent($pageId, $pageKind, Parser $parser, PPFrame $frame ) {
        $pageId = $parser->recursiveTagParse( $pageId, $frame );
        $pageId = strtolower($pageId);
        $path = self::getFileStoreDir() ."$pageId-$pageKind.html";
        //return $path;
        if(!($data = @file_get_contents($path)) && $pageKind == "content"){
            $pythonPath = self::getFileStoreDir() . "pb.py";
            $command = "/usr/bin/python3 $pythonPath $pageId";
            $command = escapeshellcmd($command);
            $res=shell_exec($command);
            //$data = file_get_contents($path);
            $data = $res;
        }
        //remove empty lines from raw HTML
        $data = str_replace("\n\n","\n", $data);
        $data = str_replace("\n","", $data);
        //$data = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $data);
        $data = self::getPBZpath($data);
        //return plain data; it must be checked before-hand by the python-script that parses the data
        return trim($data);
    }
    /**
     * get content of PBZ-report, and render it to page
     * @param $pageId
     * @param array $args
     * @param Parser $parser
     * @param PPFrame $frame
     * @return false|string
     */
    public static function renderPBZContentTag($pageId, array $args, Parser $parser, PPFrame $frame ) {
        return self::getContent($pageId, "content", $parser, $frame);
    }

    /**
     * get index of PBZ-report, and render it to page
     * @param $pageId
     * @param array $args
     * @param Parser $parser
     * @param PPFrame $frame
     * @return string
     */
    public static function renderPBZIndexTag($pageId, array $args, Parser $parser, PPFrame $frame ) {
        return "<ul>".self::getContent($pageId, "index", $parser, $frame)."</ul>";
    }
    public static function renderPBZScriptsTag($pageId, array $args, Parser $parser, PPFrame $frame ) {
        $style="<style>.zs-publication-sticky {position: -webkit-sticky;position: sticky;align-self: flex-start;top: 6em;min-width: 250px;}.zs-report-content {    border-left: solid 2px #CCC;    padding-left: 2rem;}.zs-publication > .chapter-anchor {    padding-top: 150px;    margin-top: -150px;}.chapter__link a {padding-right: 2rem;display: flex;width: 100%;margin: 1rem 0;position: relative;transition: all 250ms ease-in-out;font-size: 0.9rem;border: none !important;}.chapter__link {  list-style: none;}.chapter__link:last-of-type a {margin: 1rem 0 0 0;}.section {overflow: visible;}</style>";//
        $script="<script>RLQ.push(function () {console.log('script zs start');mw.loader.using('ext.EMM-ZS-Extension', function () {console.log('script doc-zs start');document.zs_index_function();})});</script>";
        return $style.$script;
    }

    /**
     * get image of PBZ-page, and render it to page
     * @param $pageId
     * @param array $args
     * @param Parser $parser
     * @param PPFrame $frame
     * @return string
     */
    public static function renderPBZImageTag($pageId, array $args, Parser $parser, PPFrame $frame ) {
        $pageId = $parser->recursiveTagParse( $pageId, $frame );
        return "File:ZS-".strtolower($pageId).".jpg";//
        $pageId = strtolower($pageId);
        $url = self::getFileStoreDir()."$pageId-title.txt";
        $data = file_get_contents($url);
        $img = self::getPBZpath(explode("\n", $data)[1]);
        return "<img src = '$img'/>";
    }

    /**
     * render 'emm-zs-tag' based on parameter 'part'
     * @param $pageId
     * @param array $args
     * @param Parser $parser
     * @param PPFrame $frame
     * @return array|string|string[]|null
     */
    public static function renderEmmZsTag($pageId, array $args, Parser $parser, PPFrame $frame )
    {
        $part = strtoupper($args["part"]);
        if ($part == "INDEX") {
            return self::renderPBZIndexTag($pageId, $args, $parser, $frame);
        } elseif ($part == "CONTENT") {
            return self::renderPBZContentTag($pageId, $args, $parser, $frame);
        } elseif ($part == "IMAGE") {
            return self::renderPBZImageTag($pageId, $args, $parser, $frame);
        } elseif ($part == "SCRIPT") {
            return self::renderPBZScriptsTag($pageId, $args, $parser, $frame);
        }
    }
}
