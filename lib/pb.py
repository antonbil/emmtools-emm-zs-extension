"""
get current publications of PB
"""
import urllib.request
import sys
import os
import datetime
import csv

# avoid installing all kinds of unicode libraries; of course this can be improved...
def unidecode(line):
    line = line.replace("%C3%A9", "e")
    line = line.replace("%C3%B3", "o")
    line = line.replace("%E2%80%93", "-")
    line = line.replace("%C3%A6", "e")
    line = line.replace("%C3%A8", "e")
    line = line.replace("%C3%A0", "e")
    line = line.replace("%C3%A1", "i")
    line = line.replace("%C5%82", "l")
    line = line.replace("%C5%84", "n")
    line = line.replace("%C3%BF", "y")
    line = line.replace("%C3%BE", "p")
    line = line.replace("%C3%BD", "y")
    line = line.replace("%C3%BC", "u")
    line = line.replace("%C3%BB", "u")
    line = line.replace("%C3%BA", "u")
    line = line.replace("%C3%B9", "o")
    line = line.replace("%C3%B6", "o")
    line = line.replace("%C3%B5", "o")
    line = line.replace("%C3%B4", "o")
    line = line.replace("%C3%B3", "o")
    line = line.replace("%C3%B2", "o")
    line = line.replace("%C3%B1", "n")
    line = line.replace("%C3%B0", "e")
    line = line.replace("%C3%AC", "i")
    line = line.replace("%C3%AD", "i")
    line = line.replace("%C3%AE", "i")
    line = line.replace("%C3%AF", "i")
    line = line.replace("%C3%81", "A")
    line = line.replace("%C3%82", "A")
    line = line.replace("%C3%83", "A")
    line = line.replace("%C3%84", "A")
    line = line.replace("%C3%85", "A")
    line = line.replace("%C3%86", "AE")
    line = line.replace("%C3%87", "C")
    line = line.replace("%C3%88", "E")
    line = line.replace("%C3%89", "E")
    line = line.replace("%C3%8A", "E")
    line = line.replace("%C3%8B", "E")
    line = line.replace("%C3%8C", "I")
    line = line.replace("%C3%8D", "I")
    line = line.replace("%C3%8E", "I")
    line = line.replace("%C3%8F", "I")
    line = line.replace("%C3%90", "D")
    line = line.replace("%C3%91", "N")
    line = line.replace("%C3%92", "O")
    line = line.replace("%C3%93", "O")
    line = line.replace("%C3%94", "O")
    line = line.replace("%C3%95", "O")
    line = line.replace("%C3%96", "O")
    line = line.replace("%C3%98", "O")
    line = line.replace("%C3%99", "U")
    line = line.replace("%C3%9A", "U")
    line = line.replace("%C3%9B", "U")
    line = line.replace("%C3%9C", "U")
    line = line.replace("%C3%9D", "Y")
    line = line.replace("%C3%9F", "B")
    line = line.replace("%C3%a0", "a")
    line = line.replace("%C3%a1", "a")
    line = line.replace("%C3%a2", "a")
    line = line.replace("%C3%a3", "a")
    line = line.replace("%C3%a4", "a")
    line = line.replace("%C3%a5", "a")
    line = line.replace("%C3%a6", "ae")
    line = line.replace("%C3%a7", "c")
    line = line.replace("%C3%a8", "e")
    line = line.replace("%C3%a9", "e")
    line = line.replace("%C3%aa", "e")
    #this one has been added by Anton
    line = line.replace("%C3%AA", "e")
    line = line.replace("%C3%ab", "e")
    return line

# list of current puiblications of pb
urls = [
    "https://publicaties.kczs.nl/het-burgerperspectief-op-gevaren-zeeland",
    "https://publicaties.kczs.nl/subjectief-welzijn-zeeland",
    "https://publicaties.kczs.nl/brede-welvaart-zeeland",
    "https://publicaties.kczs.nl/swipen-en-streamen",
    "https://publicaties.kczs.nl/20-jaar-jeugdmonitor-zeeland",
    "https://publicaties.kczs.nl/zeeland-nu-en-de-toekomst",
    "https://publicaties.kczs.nl/opvoeden-en-corona",
    "https://publicaties.kczs.nl/zeeland-en-vlissingen-zeven-sociaaleconomische-opgaven",
    "https://publicaties.kczs.nl/dashboard-zeeuwse-arbeidsmarkt",
    "https://publicaties.kczs.nl/evaluatie-monumentenwacht-2019"
]

def get_list():
    global urls
    urls = []
    with urllib.request.urlopen("https://publicaties.kczs.nl/") as response:
        html = response.read().decode('utf-8')
        links = html.split('<a href="https://publicaties.kczs.nl/')[1:]
        for link in links:
            my_link = "https://publicaties.kczs.nl/"+link.split('"')[0]
            # print("link:", my_link)
            urls.append(my_link)


def get_between(total_string, start, end):
    back = []
    parts = total_string.split(start)[1:]
    for part in parts:
        between = part.split(end)[0]
        between = between
        back.append(between)
    return back


def get_divs(total_string, start, add_divs = True):
    """
    get closing div based on div described in start, and return text inbetween including starting/end-div
    :param total_string:
    :param start:
    :return:
    """
    # return value
    back = []
    # define structure for start- and closing div
    div_start = ['d', 'i', 'v']
    div_end = ['/', 'd', 'i', 'v', '>']
    # get parts based on start, and remove first
    parts = total_string.split(start)[1:]
    for part in parts:
        # starting values for loop
        div_starting = False
        div_ending = False
        div_starts = 1
        # total-string to be returned
        total = ''
        # position inside start- or end-tag
        i = 0
        prev_char = 'h'
        for c in part:
            # add char to return-value
            total = total + c
            if div_starting:
                if i == len(div_start):
                    div_starts = div_starts + 1
                    div_starting = False
                    i = 0
                elif not (div_start[i] == c):
                    div_starting = False
                    i = 0
                else:
                    i = i + 1
            elif div_ending:
                if i == len(div_end):
                    # matching /div is found
                    div_starts = div_starts - 1
                    div_ending = False
                    if div_starts == 0:
                        # we found the final closing div!
                        # append to return-list, and stop loop
                        if add_divs:
                            back.append(start + total)
                        else:
                            back.append(total[:-7])
                        break
                    i = 0
                elif not (div_end[i] == c):
                    div_ending = False
                    i = 0
                else:
                    i = i + 1
            elif div_start[0] == c and prev_char == '<':
                div_starting = True
                i = 1
            elif div_end[0] == c and prev_char == '<':
                div_ending = True
                i = 1
            prev_char = c
    return back

def get_content_wikipage(id,title,date, summary):
    return """-d_open-Project|Supercontext={}
|Topcontext={}
|Toppage=Other
|Sequence number=1
|Context type=Situation
|Name={}
|Heading={}
|HeadingEn={}
|HeadingFr={}
|Show navigation tree=Nee
|Show new page button=Nee
|Show summary=Nee
|Show sub projects=Nee
|Show participants=Nee
|Show sources=Nee
|Is homepage=Nee
|Show edit button=Nee
|Show VE button=Nee
|Show category label=Nee
|Show title=Nee
-d_close--d_open-#set:zs report name|{}-d_close--d_open-#set:Start date|{}-d_close--d_open-#set:Summary|{}-d_close--d_open-ZS-Publication-d_close-
[[Category:ZS]]
   """.format(super_context, super_context, title,title,title,title, id, date, summary).replace("-d_open-", "{{").replace("-d_close-", "}}")

def get_content_resource(title, date,description,subject):
    return """
    -d_open-Resource Description
|title={}
|creator=ZS
|date={}
|hl1-openclose
|description={}
|subject={}
|-d_close-
[[Category:CS]]
    """.format(title, date,description,subject).replace("hl1-openclose","{}")


path = os.path.dirname(os.path.realpath(__file__))
db_pass = "45JbyiGbiv3j3dMlWrch"
#productie: d7f98f418ee94a5707bcd4570138c175
db_pass = "d7f98f418ee94a5707bcd4570138c175"
super_context = "PR_00353"
#supercontext on acceptatie different
#Supercontext=LC_00700
#productie: PR 00353
#print("path", path)
def get_archive(teasers, type, url, splitpoint, startlink="<li><a href=\"", endlink="</li>"):
    """
    get links for archive based on li-tags inside html-page
    :param teasers:
    :param type:
    :param url:
    :param splitpoint:
    :param startlink:
    :param endlink:
    :return:
    """
    # get summaries from https://publicaties.dezb.nl
    with urllib.request.urlopen(url) as response:
        teasers.append(["{}-{}".format(type,url)])
        html = response.read().decode('utf-8')
        html = html.split(splitpoint)[1]
        # remove titles/text added between li- and a-tag(s)
        templinks = html.split("<li>")[1]
        for tl in templinks:
            between = tl.split("<a ")[0]
            html = html.replace("<li>"+between+"<a ", "<li><a ")
        links = html.split(startlink)[1:]
        #todo: remove all from </li> to <a href=

        year = "0"
        month = "01"
        day = 28
        prev_year = "0"
        prev_month = "00"

        for link in links:
            link=link.replace("%20", " ").replace("&nbsp;", " ")
            # ref and title are at start; right after <li>-tag
            my_link = link.split(endlink)[0].strip()
            parts =my_link.split("</a>")
            anchor = parts[0]
            [ref, title] = anchor.split('">')[:2]
            if not ref.startswith("http"):
                # make sure link can be approached from outside
                ref = "https://www.dezb.nl"+ref
            summary = parts[1].replace("<br>", "")
            # remove rubbish at start of summary
            summary = summary.replace(", ", " ").replace(". ", " ").strip()
            # see if date is somewhere in ref, title or summary (in this order).....
            years = ["2021","2020","2019","2018","2017","2016","2015","2014","2013","2012","2011","2010", "2009"]
            months = ["januari","februari","maart","april","mei","juni","juli","augustus","september","oktober","november","december"]
            # help-function to extract year and month from check
            def check_date(check, year, month):
                for y in years:
                    if y in check:
                        year = y
                        break
                i = 1
                for m in months:
                    if m in check:
                        month = "{:02d}".format(i)
                    i = i + 1
                return [year,month]

            [year,month] = check_date(ref,year, month)
            [year,month] = check_date(title,year, month)
            # date in summary overwrites previous defined dates
            [year,month] = check_date(summary,year, month)
            if not(year == prev_year and month == prev_month):
                day = 29
                pass
            day = day - 1
            date = year + "/"+month+"/" + "{:02d}".format(day)
            # try to create a unique id; without strange character-tokens
            id = unidecode(ref.split("/")[-1].split(".")[0])
            if id == "pdf": # links to kb archive end with "/pdf"
                id = "{}-{}-{}".format("kb",type,unidecode(title).replace(" ", "_"))
            # add wildcard for format-function to content
            content = get_content_resource(title, date,summary,type).replace("hl1openclose", "{}")
            teasers.append([id,content,ref, title, date, summary, type])
            prev_year = year
            prev_month = month
            #
        return teasers

def add_site_url_before(content):

    return content.replace('/sites/default/files/styles/',
                           'https://publicaties.dezb.nl/sites/default/files/styles/')
def add_site_url_before_quote(content):

    return content.replace('"/sites/default/files/styles/',
                           '"https://publicaties.dezb.nl/sites/default/files/styles/')


def get_publications(urls):
    publications = []
    for url in urls:
        with urllib.request.urlopen(url) as response:
            html = response.read().decode('utf-8')
            # get sidebar with sub-anchors of current publication
            downloads = get_divs(html, '<div  class="paragraph paragraph--type-downloads">')
            download_list = []
            for d in downloads:
                publication_id = get_between(d, 'href="', '"')
                title = get_between(d, 'class="download__name">', '</div>')
                div__format = '\n<p/><div class="download-div"><a href="https://publicaties.dezb.nl{}">{}</a></div>'.format(
                    publication_id[0], title[0])
                download_list.append([publication_id[0], title[0]])

                html = html.replace(d, div__format)
            # reuse iframe after Nilsson has used div instead of iframe(emt-1910)
            power_bi = get_divs(html, '<div class="paragraph__iframe-holder" data-src="')
            for p in power_bi:
                # replace div with iframe, use height in property, and remove last /div>
                p1 = p.replace('<div class="paragraph__iframe-holder" data-src="',
                               '<iframe class="paragraph__iframe-holder" width="100%" src="')\
                    .replace("data-height","height")\
                         [:-5] + " </iframe>"
                html = html.replace(p, p1)

            heading = html.split('<ul class="publications__sidebar-menu">')[1]
            heading = heading.split("</ul>")[0]
            content = html.split('<div class="publications__content">')[1]
            # get title on top of publication
            top = content.split("<div")[0]
            # remove indexes at top; we got it anyway (in heading)
            content_start = '<div class="field field--name-field-publication-content '
            content = content.split(content_start)[1]
            content = content.split('<footer class="footer" ')[0]
            content = "{}\n{}{}".format(top, content_start, content)
            content = add_site_url_before_quote(content)
            # content = content.replace("https://publicaties.dezb.nlhttps://publicaties.dezb.nl", "https://publicaties.dezb.nl")
            pictures = content.split("<picture>")
            del pictures[0]
            for picture in pictures:
                pict_url = picture.split('<source srcset="')[1].split('"')[0]
                # print("picture:", pict_url)
                pass
            # match div and /div
            startdivs = content.split("<div")
            enddivs = content.split("</div")
            start = len(startdivs)
            end = len(enddivs)
            # remove loose end-dives
            num_loose_divs = end - start
            if num_loose_divs > 0:
                i = 0
                while i < num_loose_divs:
                    enddivs.pop()
                    i = i + 1
                content = "</div".join(enddivs)
            # add possible missing closing divs
            num_loose_divs = start - end
            if num_loose_divs > 0:
                i = 0
                while i < num_loose_divs:
                    content = content + "\n</div>"
                    i = i + 1
            # contentdl = ""
            # if len(download_list)>0:
            #    contentdl = '\n<p/><div class="heading-donwnloads">Downloads:</div>'
            # for d1 in download_list:
            #
            #    div__format = '\n<p/><div class="download-div"><a href="https://publicaties.dezb.nl{}">{}</a></div>'.format(
            #       d1[0], d1[1])
            #
            #    contentdl += div__format
            # print("dl", url, contentdl)
            # content = content + contentdl
            # get title of publication
            title = html.split('<meta property="og:title" content="')[1]
            title = title.split('" />')[0]
            # get id of current publication based on url
            id = url.split("/").pop()
            img = html.split('class="hero wrapper--center" role="presentation">')
            img = img[1].split('<source srcset="')
            img = img[1].split('?')[0]
            img = add_site_url_before(img)
            publications.append([id, title, img])
            title = title + "\n" + img

            # save publications to file, based on the id of the publication(s)
            file_name = "{}/../files/{}-{}.txt".format(path, id, "title")
            # print("file-name", file_name)
            with open(file_name, 'w') as out:
                out.write(title)
            with open("{}/../files/{}-{}.html".format(path, id, "index"), 'w') as out:
                out.write(heading)
            # print(content)
            with open("{}/../files/{}-{}.html".format(path, id, "content"), 'w') as out:
                out.write(content.replace("publicaties.dezb.nl", "publicaties.kczs.nl"))
    return publications


def get_summaries_publications(publications):
    # get summaries from https://publicaties.dezb.nl
    with urllib.request.urlopen("https://publicaties.dezb.nl") as response:
        html = response.read().decode('utf-8')
        teasers = get_divs(html, '<div class="teaser__content-wrapper">')

        file_name = "{}/bash/{}.sh".format(path, "r_bsh")
        with open(file_name, 'w') as bsh:
            for d in teasers:
                # get publication_id and summary
                publication_id = get_between(d, 'href="https://publicaties.kczs.nl/', '"')[0]
                summary = get_divs(d, '<div class="teaser__content">', False)[0].strip()
                date = get_between(d, '<div class="pre-title">', '</div>')[0].replace("januari", "01").replace(
                    "februari", "02") \
                    .replace("april", "04").replace("maart", "03").replace("mei", "05").replace("juni", "06").replace(
                    "juli", "07") \
                    .replace("augustus", "08").replace("september", "09").replace("oktober", "10").replace("october",
                                                                                                           "10") \
                    .replace("november", "11").replace("december", "12")
                date = datetime.datetime.strptime(date, '%d %m %Y').strftime('%Y/%m/%d')
                title = get_between(d, '<h3>', '</h3>')[0]
                found = False
                for p in publications:
                    if p[0] == publication_id:
                        found = True
                        p[1] = title
                        p.append(date)
                        p.append(summary)
                if not found:
                    publications.append([publication_id, title, "", date, summary])
                bsh.write("# create/update {}\n".format(publication_id))
                bsh.write(
                    'echo "{}">/tmp/test.txt\n'.format(get_content_wikipage(publication_id, title, date, summary)))
                bsh.write(
                    'php wiki/maintenance/edit.php -u anton --dbuser hzportfolio --dbpass "{}" -s "{}Quick edit" -m "ZS_publication_{}" < /tmp/test.txt\n'.format(db_pass,
                        publication_id, publication_id.replace("-", "_")))

                # save to summary-file
                file_name = "{}/../files/{}-{}.html".format(path, publication_id, "summary")
                with open(file_name, 'w') as out:
                    out.write(summary)
    return publications


# download-constant to force downloads of pdf's and add them to wiki
download_pdf = False
for arg in  sys.argv:
    if arg == "download":
        download_pdf = True
if len(sys.argv) > 1:
    argu = sys.argv[1]
    if argu=="all":
        get_list()
    elif argu == "archive":
        publications = get_publications(urls)
        publications = get_summaries_publications(publications)
        print("publications", publications)
        # archive added to call of script
        # extract links for all pages that contain archive-links
        links = get_archive([], "publication", "https://www.dezb.nl/planbureau/publicaties/rapporten.html", "Onderzoeken ZB| Planbureau 2019 en ouder")
        links = get_archive(links, "monitoring,jeugd,jonge kinderen", "https://www.dezb.nl/planbureau/jeugd/publicaties-jeugdmonitor/0-4.html", "U vindt hier publicaties van het onderzoek onder Zeeuwse ouders van kinderen die tijdens het onderzoek 3 of 4 jaar oud zijn")
        links = get_archive(links, "monitoring,jeugd, kinderen", "https://www.dezb.nl/planbureau/jeugd/publicaties-jeugdmonitor/4-12.html", "onderzoek onder groep zes kinderen van de basisschool in Zeeland")
        links = get_archive(links, "monitoring,jeugd,pubers",
                            "https://www.dezb.nl/planbureau/jeugd/publicaties-jeugdmonitor/12-18.html", "Zeeuwse derdeklassers van het voortgezet onderwijs in Zeeland")
        links = get_archive(links, "monitoring,jeugd,jongvolwassenen",
                            "https://www.dezb.nl/planbureau/jeugd/publicaties-jeugdmonitor/18-23.html", "U vindt hier de publicaties van het onderzoek onder Zeeuwse jongvolwassenen.")
        links = get_archive(links, "zbpanel",
                            "https://www.dezb.nl/wat-we-doen/zbpanel/resultaten-peilingen.html", "Overzicht resultaten van panelonderzoeken.", "<p><a href=\"", "</p>")
        file_name = "{}/bash/{}.sh".format(path, "r_bsh_archive")
        with open(file_name, 'w') as bsh:
            bsh.write("#!/bin/bash\n")
            bsh.write("# script to load all archive-items of Planbureau to wiki Projectenportfolio\n")
            bsh.write("# created by Anton Bil\n")
            bsh.write("#generated by script pb.py\n")
            bsh.write("#\n")
            bsh.write("cd /home/evm/wikis/hzportfolio\n")

            num = 0

            with open('protagonist.csv', 'w', newline='') as file:
                writer = csv.writer(file, delimiter = '\t')
                # publications.append([publication_id, title, "", date, summary])
                writer.writerow(["ID", "URL", "TITLE", "DATE","TITLE-ADDITION", "TAG", "IMAGE", "SUMMARY"])
                for p in publications:
                    try:
                      writer.writerow([ p[0], "https://publicaties.dezb.nl/"+p[0], p[1], p[3], "", "report,publication", p[2], p[4]])
                    except:
                        writer.writerow(
                            [p[0], "https://publicaties.dezb.nl/" + p[0], p[1], "", "", "report,publication", p[2], ""])
                        print("error", p)
                publication_ids = [l[0] for l in publications]
                for l in links:
                    if len(l) == 1 or l[0] in publication_ids:
                        continue
                    writer.writerow([ l[0], l[2], l[3], l[4], l[5], l[6]])#
            for l in links:
                if len(l) == 1:
                    # process header
                    bsh.write('# \n')
                    bsh.write('# page: {}\n'.format(l[0]))
                    bsh.write('# \n')
                    continue
                id = l[0]

                abc = [l[0] for l in links]
                abc = []
                for l in links:
                    abc.append(l[0])
                content = l[1]
                hyperlink = l[2]
                bsh.write("# create/update {}\n".format(id))
                # "publicaties.dezb.nl" and "toc#publication" indicate it is a link to a html-page
                if download_pdf and not ("publicaties.dezb.nl" in hyperlink or "toc#publication" in hyperlink):
                    download_id = "ZS-"+id+".pdf"
                    #  wget -O "/home/anton/drive_c/tocopy/DIO/meza/meza/emmskin-versie-2/resources/themes/zs/downloads/clientervaringsonderzoek-wet-maatschappelijke-ondersteuning-geme.pdf" -P  -e robots=off -A pdf -r -l1 "https://www.dezb.nl/dam/planbureau/bestanden/clientervaringsonderzoek-wet-maatschappelijke-ondersteuning-geme.pdf"
                    # hard-coded for acceptatie; see if it the same on production..
                    download_dir = "/home/evm/mediawiki/extensions/EMM-PBZ-extension/downloads"
                    bsh.write('wget -O "{}/{}" -P  -e robots=off -A pdf -r -l1 "{}"\n'.format(download_dir, download_id,hyperlink))
                    bsh.write('php wiki/maintenance/importImages.php {} --comment="Importing files from local file repository" --user=anton --overwrite --extensions=pdf\n'.format(download_dir))
                    bsh.write('rm "{}/{}"\n'.format(download_dir, download_id))
                    hyperlink = "file name="+download_id
                else:
                    hyperlink = "hyperlink="+hyperlink
                # add hyperlink to content
                content = content.format(hyperlink)
                # add template-opening and closing-tags
                # can only be added after the string has been formatted....
                content = content.replace("-d_open-", "{{").replace("-d_close-", "}}")
                content = urllib.parse.unquote(content.strip()).replace("[","").replace("]","").replace("([)","").replace(")","").replace("[","")
                bsh.write('echo "{}">/tmp/test.txt\n'.format(content))
                bsh.write('php wiki/maintenance/edit.php -u anton --dbuser hzportfolio --dbpass "45JbyiGbiv3j3dMlWrch" -s "{}Quick edit" -m "Resource_Description_CS_{}" < /tmp/test.txt\n'.format(id,id.replace("-", "_")))
                # give the server some rest...
                bsh.write("sleep 1\n")
                num = num + 1

        #
        print("number of links:", num)
        exit(0)
    else:
        arg1 = "https://publicaties.dezb.nl/" + argu
        urls = [arg1]
        os.chdir(path)


publications = get_publications(urls)

publications = get_summaries_publications(publications)
# rapporten: downloaden, en resource description maken?
# https://www.dezb.nl/planbureau/publicaties/rapporten.html
# alleen interessant die rapporten die starten met: dam/....
# het is wel een zootje: de urls beginnen met dam, planbureau, https://www.dezb.nl/dam
# en er zijn er minstens 3 die doorverwijzen naar http://toegang.kb.nl, die worden via een 2e page downloaden
# vragen: - oorspronkelijke directory-structuur van belang?
# - bestanden uit kb.nl ook downloaden?
# date -: split op , dan trim op ., en kijk of bevat: 2021, ... 2015--> dat is de datum als korter dan ... char...
# jeugd-monitor: bestaat uit hoofd-pagina die uitsplitsts naar 4 pagina's met lijsten, de onderste (laatste, meestal ouder dan 2013 / 2011) verwijzen naar kb.nl
# daar staat geen datum bij, alleen een url en een beschrijving (in de titel)
# samenvattend: 5 pagina's met bronnen, die zijn te downloaden, en daarvoor zijn resource-descriptions te maken met file, en beschrijving, en soms een datum.
# ze moeten wel in volgorde worden bewaard; jaar eraan gekoppeld?
# voor jeugd-monitor is dat erg lastig; dat kan denk ik het handigst door het handmatig langs te lopen voordat de resource-loader wordt geupload.
# https://www.dezb.nl/wat-we-doen/zbpanel/resultaten-peilingen.html
# https://www.mediawiki.org/wiki/Manual:ImportImages.php
# The same as above but only overwriting the given image types, e.g. pdf (for .pdf-files):
#
#  wget -O "/home/anton/drive_c/tocopy/DIO/meza/meza/emmskin-versie-2/resources/themes/zs/downloads/clientervaringsonderzoek-wet-maatschappelijke-ondersteuning-geme.pdf" -P  -e robots=off -A pdf -r -l1 "https://www.dezb.nl/dam/planbureau/bestanden/clientervaringsonderzoek-wet-maatschappelijke-ondersteuning-geme.pdf"
# php /home/evm/wikis/hzportfolio$ php wiki/maintenance/importImages.php /home/evm/mediawiki/extensions/EMM-PBZ-extension/downloads --comment="Importing files from local file repository" --user=anton --overwrite --extensions=pdf
# dan is File:Clientervaringsonderzoek-wet-maatschappelijke-ondersteuning-geme.pdf aangemaakt

