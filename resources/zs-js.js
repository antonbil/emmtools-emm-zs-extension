document.zs_index_function = ()=>{
  //console.log("inside function zs_index_function");
        console.log("starting function....");
      let lastId,
          fromTop,
          current;

      const topMenu = $('.header'),
          topMenuHeight = topMenu.outerHeight() + 15,
          index = $('.zs-publication-sticky'),
          menuItems = index.find('a');

      const scrollItems = menuItems.map(function () {
        const item = $($(this).attr('href'));
        if (item.length) return item;
      });

      $(window).scroll(function () {
        fromTop = $(this).scrollTop() + topMenuHeight;
        current = scrollItems.map(function () {
          if ($(this).offset().top < fromTop) {
            return this;
          }
        });

        current = current[current.length - 1];
        const id = current && current.length ? current[0].id : '';
        if (lastId !== id) {
          lastId = id;
          menuItems.parent().removeClass('chapter__link--active').end().filter("[href='#" + id + "']").parent().addClass('chapter__link--active');
        }
      });
}
